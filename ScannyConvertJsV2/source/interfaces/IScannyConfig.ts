export interface IScannyConfig {
    useNgRok : boolean;
    port : number;
    ipAddress: string;
}