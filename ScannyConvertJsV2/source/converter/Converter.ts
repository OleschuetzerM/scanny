import request = require("request-promise");
import * as fs from "fs";
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export class Converter {
    public async processInput(path: string): Promise<string> {
        var fileData = fs.readFileSync(path);
        return this.processInputBuffer(fileData);
    }

    public async processInputBuffer(buffer: Buffer): Promise<string> {
        let locationHeader: string = await this.requestSendPicture(buffer);
        return await this.requestParsedInput(locationHeader);
    }

    private async requestSendPicture(buffer: Buffer): Promise<string> {
        return new Promise(function (resolve: any, reject: any) {
            var options: request.Options = {
                url: ConverterConfig.subscriptionUri,
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'Ocp-Apim-Subscription-Key': ConverterConfig.subscriptionKey,
                },
                body: buffer
            };
            request.post(options, function (error, response, body) {
                if (error) {
                    reject(new Error(error));
                }
                else if (body && JSON.parse(body).error) {
                    reject(new Error(body));
                }
                else {
                    var locationHeader: any = <String>response.headers['operation-location'];
                    resolve(locationHeader);
                }
            });
        });
    }

    private async requestParsedInput(locationHeader: string): Promise<string> {
        return new Promise(async function (resolve: any, reject: any) {
            let attempts = 25;
            let counter = 0;
            let opt: request.Options = {
                url: locationHeader,
                headers: {
                    'Ocp-Apim-Subscription-Key': ConverterConfig.subscriptionKey
                }
            };

            let result: string = "";
            while (counter < attempts && !result.includes("Succeeded")) {
                counter++;
                await sleep(350);
                result = await request.get(opt);
            }
            if (result.includes("Succeeded"))
                resolve(result);
            else
                reject(new Error("Result was not successfull after 10 attempts"))
        });
    }
}

class ConverterConfig {
    public static subscriptionKey: String = "ab3730d2225645a2a794426c4d23f668";
    public static subscriptionUri = "https://westcentralus.api.cognitive.microsoft.com/vision/v2.0/recognizeText?mode=Handwritten";
}