import { ScannyWord } from './ScannyWord';
export class ScannyLine {
    public text : string;
    public words : Array<ScannyWord> = new Array<ScannyWord>();
}