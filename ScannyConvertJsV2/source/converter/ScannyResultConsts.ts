export class ScannyResultConsts {
    public static result : string = "recognitionResult";
    public static lines : string = "lines";
    public static lineWords : string = "words";
    public static lineText : string = "text";
    public static wordConfidence : string = "confidence";
    
}