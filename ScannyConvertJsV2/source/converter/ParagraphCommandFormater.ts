import { debug } from "util";

export class ParagraphCommandFormater {   
    private styles = {
        bold: "bold",
        italic: "italic",
        underline: "underline",
        heading: [
            "h1",
            "h2",
            "h3"
        ]
    }
    
    constructor(private document : any) {
        this.init();
    }

    private init() {
        this.document.Styles.createParagraphStyle('h1', 'h1')
        .basedOn("Normal")
        .next("Normal")
        .quickFormat()
        .color("2e74b5")
        .size(40)
        .spacing({after: 120});

        this.document.Styles.createParagraphStyle('h2', 'h2')
        .basedOn("Normal")
        .next("Normal")
        .color("2e74b5")
        .size(34)
        .spacing({before: 240, after: 120});


        this.document.Styles.createParagraphStyle('h3', 'h3')
        .basedOn("Normal")
        .next("Normal")
        .quickFormat()
        .size(28)
        .color("2e74b5")
        .bold()
        .spacing({before: 240, after: 120});
        

        this.document.Styles.createParagraphStyle(this.styles.italic, this.styles.italic)
        .basedOn('Normal')
        .next('Normal')
        .italics()
        this.document.Styles.createParagraphStyle(this.styles.bold, this.styles.bold)
        .basedOn('Normal')
        .next('Normal')
        .bold()
        this.document.Styles.createParagraphStyle(this.styles.underline, this.styles.underline)
        .basedOn('Normal')
        .next('Normal')
        .underline();
    }

    public format(paragraph : any, text : string) : void {
        let str : string = text.replace(/\s/g, "").toLowerCase();
        this.checkHeadings(paragraph, str);
        this.checkboldItalicUnderline(paragraph, str);
    }

    private checkHeadings(paragraph : any, text : string) : void {
        for(let i = 0; i < 3; i++) {
            if(text.includes(`#${i}#`) === true) {
                paragraph.style(this.styles.heading[i])
            }
        }
    }

    private checkboldItalicUnderline(paragraph : any, text : string) : void {
        if(text.includes(`#u#`) === true) {
            paragraph.style(this.styles.underline);
        }
        if(text.includes(`#i#`) === true) {
            paragraph.style(this.styles.italic);
        }
        if(text.includes(`#b#`) === true) {
            paragraph.style(this.styles.bold);
        }
    }
}