import { ScannyResult } from "./ScannyResult";
import { ScannyLine } from "./ScannyLine";
import { ScannyWord } from "./ScannyWord";
import { ParagraphCommandFormater } from "./ParagraphCommandFormater";
import { debug } from "util";

const docxGenerator = require("docx");
const fs = require('fs');
const pathSystem = require("path");

export class OfficeGenerator {
  public async generate(path : string, scannyResult : ScannyResult, useCommands : boolean) : Promise<Buffer> {
    return new Promise(function(resolve, reject) {
        const docx = new docxGenerator.Document({
          creator: 'Scanny Reader by Fruhmann',
          title: 'Scanny Document',
          description: 'A computer generated document by scanny',
      });

      const formatter : ParagraphCommandFormater = new ParagraphCommandFormater(docx);
        scannyResult.scannyLines.forEach((line : ScannyLine)  => {
          let para = docx.createParagraph();
          if(useCommands === true) 
              formatter.format(para, line.text);
          line.words.forEach((word : ScannyWord) => {
            let col : string = ""; 
                if(word.confidence !== undefined && word.confidence.toLowerCase() == "low") {
                    col = "ff0000";
                }
                else {
                    col = "000000";
                }
                para.createTextRun(`${word.text} `).color(col);
            });
        });

        const packer = new docxGenerator.Packer();
        packer.toBase64String(docx).then((b64str : string) => {
          resolve(Buffer.from(b64str, 'base64'));
        });
      }); 
    }
  }
