import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";


@Entity()
export class ScannyWordDocument {
    constructor(data : Buffer) {
        this.wordData = data;
        this.createdOn = new Date();
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "bytea", nullable: true})
    wordData : Buffer;

    @Column()
    createdOn : Date;
}