import { Converter } from './converter/Converter';
import { ScannyResult } from './converter/ScannyResult';
import express = require("express");
import fileUpload = require("express-fileupload");
import {createConnection, Connection} from "typeorm";
import { RouteFactory } from './routes/RouteFactory';
import ngrok = require('ngrok');
import fs = require('fs');
import path = require('path');
import { IScannyConfig } from './interfaces/IScannyConfig';
const exphbs  = require('express-handlebars');

const config : IScannyConfig = JSON.parse(<any>fs.readFileSync(path.resolve('./scannyconfig.json')));
console.log("Loaded config");
console.log(JSON.stringify(config));
const port = config.port;
const ip = config.ipAddress;

// Use Handlebars view engine
createConnection().then((conn : Connection) => {
    express()
    .engine('.hbs', exphbs({defaultLayout: 'main', extname: '.hbs'}))
    .set('view engine', '.hbs')
    .set('views', path.join(__dirname, '../views'))
    .use(fileUpload({
        limits: { fileSize: 150 * 1024 * 1024 },
    }))
    .use("/index", (req, res) => res.render("index"))
    .use(express.static(path.join(__dirname, '../views/public')))
    .use("/documents", RouteFactory.GetDocuments(conn).router)
    .listen(port, ip, () => console.log(`Server is up and running on port ${port} . . . `));
    if(config.useNgRok === true)
        ngrok.connect(port).then((url : string) => console.log(url));
})
.catch((err) => {
    console.log(`Error connecting to db: ${err}`);
});

/*
var tunnel = localtunnel(port, function(err : any, tunnel : any) {
    if (err) {
            console.log(`Error creating tunnel ${err}`);
    }
    else {
            console.log(tunnel.url);
    }
});
*/