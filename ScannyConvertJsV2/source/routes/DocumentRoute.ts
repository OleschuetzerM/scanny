import { Router, Request, Response } from "express";
import { UploadedFile } from "express-fileupload";
import { Converter } from '../converter/Converter';
import { ScannyResult } from '../converter/ScannyResult';
import { Connection, Repository } from "typeorm";
import { ScannyWordDocument } from "../domain/ScannyWordDocument";
import sharp = require('sharp');
import { reject } from "bluebird";
import { debug } from "util";

export class DocumentRoute {
    public router = Router();
    private repo: Repository<ScannyWordDocument>;

    constructor(conn: Connection) {
        this.repo = conn.getRepository(ScannyWordDocument);

        this.router.get("/", async (req, res) => {
            let data = this.repo.find();
            res.status(200).send(JSON.stringify(data));
        });

        this.router.get("/:id", this.getById.bind(this));
        this.router.post("/", this.postDocumentCmd.bind(this));
        this.router.post("/raw", this.postDocumentRaw.bind(this));
    }

    private async getById(req: Request, res: Response) {
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        res.setHeader('Content-disposition', 'attachment; filename=scannydoc.docx');

        if (!req.params.id) return res.status(400).end("bad request");

        var result = await this.repo.findOne(req.params.id);
        res.end(result.wordData);
    }

    private async postDocumentCmd(req: Request, res: Response) {
        this.postDocument(req, res, true);
    }

    private async postDocumentRaw(req: Request, res: Response) {
        this.postDocument(req, res, false);
    }

    private async postDocument(req: Request, res: Response, isCommanded: boolean) {
        const converter: Converter = new Converter();
        let imageData: Buffer = (<UploadedFile>req.files.file).data;
        const scanResult = new ScannyResult();
        imageData = await this.tryResizeImage(imageData, 10);
        try {
            const data = await converter.processInputBuffer(imageData);
            scanResult.parseData(data);

            scanResult.toWordFile("./", true, async (buffer: Buffer) => {
                var doc = new ScannyWordDocument(buffer);
                await this.repo.save(doc);
                res.end(`${doc.id}`);
            },
                (err: any) => {
                    res.status(500).end("Internal Server Error");
                    console.log(`Error converting scanny result to word file ${err} ${(<Error>err).stack}`);
                });
        } catch (err) {
            console.log(`Error sending pciture to server ${err} ${(<Error>err).stack}`);
            res.status(500).end();
        }
    }

    private async tryResizeImage(data: Buffer, iterations: number): Promise<Buffer> {
        return new Promise(async function (resolve: any, reject: any) {
            if (toMegaByte(data.byteLength) > 4) {
                let imageData : Buffer = data;
                let sharpData : sharp.Sharp;
                let metadata : sharp.Metadata;
                let counter = 0;
                while (counter < iterations && toMegaByte(imageData.byteLength) >= 4) {
                    counter++;

                    sharpData = sharp(imageData)
                    metadata = await sharpData.metadata()
                    imageData = await sharpData
                        .resize(Math.round(metadata.width * 0.9), Math.round(metadata.width * 0.9)).toBuffer();
                    console.log(`${imageData.byteLength} Bytes = ${toMegaByte(imageData.byteLength)} Mb`)
                }
                if (toMegaByte(imageData.byteLength) < 4)
                    resolve(imageData);
                else
                    reject(new Error(`File size could not be reduced to < 4mb in ${iterations} iterations`))
            }
            else {
                resolve(data);
            }
        });
        function toMegaByte(bytes: number): number {
            return bytes * 0.000001;
        }
    }
}
