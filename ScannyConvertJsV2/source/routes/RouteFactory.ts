import { DocumentRoute } from "./DocumentRoute";
import { Connection } from "typeorm";

export class RouteFactory {
    static GetDocuments(conn : Connection) : DocumentRoute {
        return new DocumentRoute(conn);
    }    
}