document.addEventListener("DOMContentLoaded", function(event) {
    console.log("DOM fully loaded and parsed");
    
    const form1 = document.forms.item(0);
    form1.addEventListener('submit', (ev) => listener(ev, "/documents", form1), false);
    const form2 = document.forms.item(1);
    form2.addEventListener('submit', (ev) => listener(ev, "/documents/raw", form2), false);
});

function listener(ev, url, form) {
    var oOutput = document.querySelector("div"),
    oData = new FormData(form);
    var oReq = new XMLHttpRequest();
    oReq.open("POST", url, true);
    oReq.onload = function(oEvent) {
        if (oReq.status == 200) {
            confirmDownload(oReq.responseText);
        } else {
            alert("Error " + oReq.status + " occurred when trying to upload your file.");
        }};
    oReq.send(oData);
    ev.preventDefault();
}

function confirmDownload(id) {
    if(confirm(`Document ready. ID: ${id}`)) {
        window.location.href = `../documents/${id}`;
    }
}
