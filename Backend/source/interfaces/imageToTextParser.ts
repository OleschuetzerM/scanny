export interface ImageToTextParser {
    imageToRawText(imageData: Buffer): Promise<string>;
    imageToWordDoc(imageData: Buffer): Promise<Buffer>;
}