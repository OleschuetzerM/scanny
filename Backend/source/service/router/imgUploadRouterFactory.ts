import { FileManager } from '../../logic/fileManager';
import { Router } from 'express';
import { ImageToTextParser } from '../../interfaces/imageToTextParser';
import { UploadedFile } from 'express-fileupload';
import Path from 'path';
import FileSystem from 'fs';

export class ImgUploadRouterFactory {

    public static create(manager: FileManager, imgParser: ImageToTextParser): Router {
        const router = Router();
        let idx = 1;
        router.post("/upload", async (req, res) => {
            const file: UploadedFile = <any>req.files.pic;
            const content = imgParser.imageToRawText(file.data);
            FileSystem.writeFileSync(Path.normalize(`${__dirname}/../public/file${idx}.txt`), content, {encoding: "utf8"});
            res.end("Success");
        });

        return router;
    }
}