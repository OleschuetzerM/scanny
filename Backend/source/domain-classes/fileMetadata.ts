import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import FileSystem from "fs";
import Path from "path";
import { User } from './user';

@Entity()
export class FileMetadata {
    
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public filename: string;

    @Column({unique: true})
    public path: string;

    @ManyToOne(type => User, user => user.documents)
    public owner: User;
}