import { Entity, ManyToOne, PrimaryColumn, OneToMany } from "typeorm";
import { FileMetadata } from './fileMetadata';

@Entity()
export class User {
    @PrimaryColumn()
    public username: string;

    @OneToMany(type => FileMetadata, file => file.owner)
    public documents: FileMetadata[];
}