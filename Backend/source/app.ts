import Path from 'path';
import express = require("express");
import bodyParser = require("body-parser");
import cookieParser = require("cookie-parser");
import fileUpload = require("express-fileupload");
import morgan = require("morgan");
import { createConnection } from 'typeorm';
import { ImgUploadRouterFactory } from './service/router/imgUploadRouterFactory';
import { FileManager } from './logic/fileManager';
import { ImageToTextParser } from './interfaces/imageToTextParser';

const port = 8080;
const app = express();

const publicFolder = Path.normalize(`${__dirname}/../public`);

app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(fileUpload());

// DEBUG:
app.get("/", (req, res) => {
    res.sendFile(Path.normalize(__dirname + "/../public/index.html"));
});

createConnection().then(async dbConnection => {
    await dbConnection.synchronize();
    
    const fileManager = new FileManager();
    const imgParser: ImageToTextParser = null;  // TODO: Instanciate image parser
    app.use("/file", ImgUploadRouterFactory.create(fileManager, imgParser));

    app.listen(port, () => {
        console.log(`Sever up and listening on http://localhost:${port}/ . . .`);
    })
}).catch(console.error); 