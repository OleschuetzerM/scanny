"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");
const morgan = require("morgan");
const typeorm_1 = require("typeorm");
const imgUploadRouterFactory_1 = require("./service/router/imgUploadRouterFactory");
const fileManager_1 = require("./logic/fileManager");
const port = 8080;
const app = express();
const publicFolder = path_1.default.normalize(`${__dirname}/../public`);
app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(fileUpload());
// DEBUG:
app.get("/", (req, res) => {
    res.sendFile(path_1.default.normalize(__dirname + "/../public/index.html"));
});
typeorm_1.createConnection().then((dbConnection) => __awaiter(this, void 0, void 0, function* () {
    yield dbConnection.synchronize();
    const fileManager = new fileManager_1.FileManager();
    const imgParser = null; // TODO: Instanciate image parser
    app.use("/file", imgUploadRouterFactory_1.ImgUploadRouterFactory.create(fileManager, imgParser));
    app.listen(port, () => {
        console.log(`Sever up and listening on http://localhost:${port}/ . . .`);
    });
})).catch(console.error);
//# sourceMappingURL=app.js.map