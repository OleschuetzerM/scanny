import { Connection, Repository } from "typeorm";
import { Router, Request, Response } from "express";
import { User } from "../domains/user";
import { SessionStorage } from "../logic/sessionStorage";

export class AccountRouterFactory {
    private data: Repository<User> = null;

    private async findUser(email: string): Promise<User> {
        return await this.data.findOne({ where: { email: email } }) || null;
    }

    private generateRandomString(length: number): string {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    private async setCookie(res: Response, user: User): Promise<void> {
        const sessionId = this.generateRandomString(10);
        res.cookie("sessionId", sessionId);

        user.sessionToken = sessionId;
        await this.data.save(user);
    }

    private clearCookies(res: Response) {
        res.clearCookie("sessionId");
    }

    private async register(req: Request, res: Response) {
        try {
            const user: User = new User();
            try {
                user.fromObj(req.body);
            } catch (err) {
                return res.status(400).json({err: err.message, given: user});
            }

            const persisted = await this.findUser(user.email);
            if (persisted != null) {
                return res.status(409).end(`User with email ${user.email} is already registered!`);
            }

            await this.data.save(user);
            await this.login(req, res);
        } catch (err) {
            return res.status(500).end(err.message);
        }
    }

    private async login(req: Request, res: Response) {
        try {
            if (req.cookies.sessionId) {
                return res.status(200).end("Already logged in!");
            }
    
            const credentials: User = new User();
            try {
                credentials.fromObj(req.body);
            } catch (err) {
                return res.status(400).json({err: err.message, given: credentials});
            }
    
            const persisted = await this.findUser(credentials.email);
            if (persisted == null) {
                return res.status(405).end(`Email ${credentials.email} registered!`);
            } else if (persisted.password != credentials.password) {
                return res.status(405).end(`Wrong password`);
            }
    
            await this.setCookie(res, persisted);
            SessionStorage.sessions[persisted.sessionToken] = persisted;

            return res.status(200).end("Successfully logged in!");
        } catch (err) {
            return res.status(500).end(err.message);
        }
    }

    private logout(req: Request, res: Response) {
        try {
            delete SessionStorage.sessions[req.cookies.sessionId];
            this.clearCookies(res);
            return res.status(200).end("Logged out!");
        } catch (err) {
            return res.status(500).end(err.message);
        }
    }

    public create(connection: Connection): Router {
        this.data = connection.getRepository(User);
        const that = this;
        return Router()
            .post("/register", async (req, res) => await this.register(req, res))
            .post("/login", async (req, res) => await this.login(req, res))
            .get("/logout", async (req, res) => await this.logout(req, res));
    }
}