import { Entity, PrimaryColumn, Column, OneToMany } from "typeorm";
import { Document } from "./document";

@Entity()
export class User {
    @PrimaryColumn()
    public email: string;

    @Column()
    public password: string;

    @OneToMany(type => Document, doc => doc.owner)
    public documents: Document[];

    @Column()
    public sessionToken: string;

    public fromObj(obj: any) {
        if (!obj.email) {
            throw new Error("Email has to be specified!");
        }

        if (!obj.password) {
            throw new Error("Password has to be specified!");
        }

        this.email = obj.email;
        this.password = obj.password;
        this.documents = obj.document || [];
        this.sessionToken = obj.sessionToken || [];
    }
}