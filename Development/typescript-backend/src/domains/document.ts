import { User } from "./user";
import { PrimaryColumn, Column, ManyToOne, Entity } from "typeorm";

@Entity()
export class Document {
    @PrimaryColumn()
    public title: string;

    @Column()
    public content: string;

    @ManyToOne(type => User, user => user.documents)
    public owner: User;

    public fromObj(obj: any) {
        if (!obj.title) {
            throw new Error("Title has to be specified!");
        }

        if (!obj.content) {
            throw new Error("Content has to be specified!");
        }

        if (!obj.owner) {
            throw new Error("Owner has to be specified!");
        }

        this.title = obj.title;
        this.content = obj.content;
        this.owner = obj.owner;
    }
}