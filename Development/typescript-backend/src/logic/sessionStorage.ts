import { User } from "../domains/user";

export class SessionStorage {
    public static sessions: { [key: string]: User } = {};
}