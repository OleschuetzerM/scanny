import "reflect-metadata";
import express = require("express");
import { createConnection } from "typeorm";
import { AccountRouterFactory } from "./service/accountRouterFactory";
import cookieParser = require("cookie-parser");
import bodyParser = require("body-parser");
import morgan = require("morgan");

const port = 8080;

createConnection().then(connection => {
    connection.synchronize(true);

    const app = express()

        // Configure Middleware:
        .use(morgan("combined"))
        .use(cookieParser())
        .use(bodyParser.json())

        // Register Router:
        .use("/accounts", new AccountRouterFactory().create(connection))

        // Fire up Server:
        .listen(port, () => 
            console.log(`Server up and running on http://localhost:${port} . . .`));
}).catch(err => {
    console.error(`Failed to start Server!`);
    console.error(err);
})