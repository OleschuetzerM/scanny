package interfaces;

public interface Domain {
	int getId();
	void setId(int id);
	
	String toJson();
	
	void fromJson(String source);
	
	Object getShallowCopy();
}
