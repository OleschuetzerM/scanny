package domains;

import interfaces.Domain;

public class ScannyDocumentShare implements Domain {
	private ScannyDocument document = null;
	private ScannyUser owner = null;
	private ScannyUser shareReceiver = null;
	private int id = -1;
	public ScannyDocument getDocument() {
		return document;
	}
	public void setDocument(ScannyDocument document) {
		this.document = document;
	}
	public ScannyUser getOwner() {
		return owner;
	}
	public void setOwner(ScannyUser owner) {
		this.owner = owner;
	}
	public ScannyUser getShareReceiver() {
		return shareReceiver;
	}
	public void setShareReceiver(ScannyUser shareReceiver) {
		this.shareReceiver = shareReceiver;
	}
	
	@Override
	public int getId() {
		return this.id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
		
	}
	
	
	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void fromJson(String source) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getShallowCopy() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
