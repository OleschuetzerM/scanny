package domains;

import java.util.ArrayList;
import java.util.List;

import dataClasses.Credentials;
import interfaces.Domain;

public class ScannyUser implements Domain {
	private String identifier = null;
	private String name = null;
	private Credentials credentials = null;
	private List<ScannyDocument> documents = new ArrayList<ScannyDocument>();
	private int id = -1;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public Credentials getCredentials() {
		return credentials;
	}
	public void setCredentials(Credentials credentials) {
		this.credentials = credentials;
	}
	public List<ScannyDocument> getDocuments() {
		return documents;
	}
	public void setDocuments(List<ScannyDocument> documents) {
		this.documents = documents;
	}
	
	@Override
	public int getId() {
		return this.id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
		
	}
	
	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void fromJson(String source) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getShallowCopy() {
		// TODO Auto-generated method stub
		return null;
	}

}
