package domains;

import interfaces.Domain;

public class ScannyDocument implements Domain {
	private String title = null;
	private String fileName = null;
	private ScannyUser user = null;
	private Category category = null;
	private byte[] content = null;
	private int id = -1;
	
	public byte[] getContent() {
		return this.content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public ScannyUser getUser() {
		return this.user;
	}
	public void setUser(ScannyUser user) {
		this.user = user;
	}
	public Category getCategory() {
		return this.category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	@Override
	public int getId() {
		return this.id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
		
	}
	
	
	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void fromJson(String source) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object getShallowCopy() {
		// TODO Auto-generated method stub
		return null;
	}
}
