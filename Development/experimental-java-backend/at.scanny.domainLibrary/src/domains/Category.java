package domains;

import interfaces.Domain;

public class Category implements Domain{
	private int id = -1;
	private String text = null;
	
	@Override
	public int getId() {
		return this.id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
	}
	
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
		
	}
	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromJson(String source) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getShallowCopy() {
		// TODO Auto-generated method stub
		return null;
	}
}
