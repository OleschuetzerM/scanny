package facade;

import exceptions.NotInitializedException;
import framework.databaseAccess.IConnectionFactory;

public class Logic {
	private static Logic instance = null;
	private boolean isInitialized = false;
	
	private DomainProcessesContainer processes = null;
	
	private Logic() {}
	
	public static Logic instance() {
		if (instance == null) {
			instance = new Logic();
		}
		
		return instance;
	}
	
	public void initialize(IConnectionFactory factory) throws Exception {
		this.processes = new DomainProcessesContainer(factory);
		
		this.isInitialized = true;
	}
	
	public DomainProcessesContainer processes() throws NotInitializedException {
		if (!this.isInitialized) {
			throw new NotInitializedException("Logic not initialized!");
		}
		
		return this.processes;
	}
	
}