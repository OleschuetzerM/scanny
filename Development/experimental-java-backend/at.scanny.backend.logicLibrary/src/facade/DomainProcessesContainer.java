package facade;

import businessProcesses.DocumentProcesses;
import businessProcesses.UserProcesses;
import framework.databaseAccess.IConnectionFactory;

public class DomainProcessesContainer {
	private UserProcesses users = null;
	private DocumentProcesses documents = null;

	DomainProcessesContainer(IConnectionFactory factory) throws Exception {
		users = new UserProcesses(factory);
		documents = new DocumentProcesses(factory);
	}
	
	public UserProcesses users() {
		return this.users;
	}
	
	public DocumentProcesses documents() {
		return this.documents;
	}
}
