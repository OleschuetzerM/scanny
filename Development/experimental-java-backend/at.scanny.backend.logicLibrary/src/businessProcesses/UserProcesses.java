package businessProcesses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dataClasses.Credentials;
import domains.ScannyUser;
import framework.databaseAccess.IConnectionFactory;

public class UserProcesses {
	private IConnectionFactory factory = null;
	private Connection connection = null;
	
	private final String NAME_SELECT = "select username, passhash, salt, identifier from scanny_user where fullname = ?";
	private final String IDENTIFIER_SELECT = "select fullname, username, passhash, salt from scanny_user where identifier = ?";
	private final String USERS_DOCUMENTS = null;
	
	private PreparedStatement selectByName = null;
	private PreparedStatement selectByIdentifier = null;
	private PreparedStatement selectUsersDocuments = null;
	
	public UserProcesses(IConnectionFactory factory) throws Exception {
		super();
		this.factory = factory;
		this.connection = this.factory.getConnection();
		
		this.selectByName = this.connection.prepareStatement(this.NAME_SELECT);	
		this.selectByIdentifier = this.connection.prepareStatement(this.IDENTIFIER_SELECT);
		this.selectUsersDocuments = this.connection.prepareStatement(this.USERS_DOCUMENTS);
	}
	
	public List<ScannyUser> getByName(String name) throws SQLException {
		List<ScannyUser> results = new ArrayList<ScannyUser>();
		
		this.selectByName.setString(1, name);
		ResultSet rset = this.selectByName.executeQuery();
		while (rset.next()) {
			ScannyUser u = new ScannyUser();
			
			String username = rset.getString(1);
			String passHash = rset.getString(2);
			String salt = rset.getString(3);
			
			Credentials c = new Credentials(username, passHash, salt);
			
			u.setCredentials(c);
			u.setIdentifier(rset.getString(4));
			u.setName(name);
			
			results.add(u);
		}
		rset.close();
		
		return results;
	}
}
