package framework.databaseAccess;
import java.sql.*;

public interface IDataAccessObject {
    Object create(Object objToCreate) throws SQLException;
    Object get(int id) throws SQLException;
    ResultSet get() throws SQLException;
    boolean delete(int id) throws SQLException;
    Object update(Object objToUpdate) throws SQLException;   
}
