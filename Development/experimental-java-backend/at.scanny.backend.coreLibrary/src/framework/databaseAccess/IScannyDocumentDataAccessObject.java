package framework.databaseAccess;

import java.sql.SQLException;

import domains.ScannyDocument;

public interface IScannyDocumentDataAccessObject extends IDataAccessObject {
	ScannyDocument loadContent(ScannyDocument document) throws SQLException;
}
