package framework.databaseAccess;

import java.sql.Connection;

public interface IConnectionFactory {
	Connection getConnection() throws Exception;
}
