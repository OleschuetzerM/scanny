package dataAccess;

import java.sql.*;

import domains.Category;
import domains.ScannyDocument;
import domains.ScannyUser;
import framework.databaseAccess.IDataAccessObject;
import framework.databaseAccess.IScannyDocumentDataAccessObject;

public class DataAccessDocument implements IScannyDocumentDataAccessObject {
	public static DataAccessDocument instance = new DataAccessDocument();
	
	private Connection connection = null;
	private PreparedStatement stmtGetAll = null;
	private String sqlGetAll = "select s.* from ScannyDocument s";
	
	private PreparedStatement stmtCreate = null;
	private String sqlCreate = "insert into ScannyDocument (userid, categoryid, content) values (?, ?, ?)";
	
	private PreparedStatement stmtUpdate = null;
	private String sqlUpdate = "update ScannyDocument set userid=? categoryid=? content=?";
	
	private PreparedStatement stmtDelete = null;
	private String sqlDelete = "delete from ScannyDocument where id=?";
	
	private PreparedStatement stmtGetById = null;
	private String sqlGetById = "select s.* from ScannyDocument s where s.id=?";

	private PreparedStatement stmtGetContent= null;
	private String sqlGetContent = "select s.content from ScannyDocument s where s.id=?";
	
	private DataAccessDocument() {
		try {
			connection = ConnectionFactory.instance.getConnection();
			stmtGetAll = connection.prepareStatement(sqlGetAll);
			stmtCreate = connection.prepareStatement(sqlCreate);
			stmtUpdate = connection.prepareStatement(sqlUpdate);
			stmtDelete = connection.prepareStatement(sqlDelete);
			stmtGetById = connection.prepareStatement(sqlGetById);
			stmtGetContent = connection.prepareStatement(sqlGetContent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public Object create(Object objToCreate) throws SQLException {
		ScannyDocument doc = (ScannyDocument)objToCreate;
		setParams(stmtCreate, doc);
		ResultSet rs = stmtCreate.getGeneratedKeys();
	
		if (rs.next()) 
		    doc.setId(rs.getInt(1));
		
		stmtCreate.clearParameters();
		return null;
	}

	@Override
	public Object get(int id) throws SQLException {
		setIdParam(stmtGetById, id);
		ResultSet rs = stmtGetById.executeQuery();
		Object ret = null;
		if(rs.next())
			ret = parseObject(rs);
		return ret;
	}

	public ScannyDocument loadContent(ScannyDocument document) throws SQLException {
		setIdParam(stmtGetContent, document.getId());
		ResultSet rs = stmtGetContent.executeQuery();
		ScannyDocument ret = null;
		if(rs.next()) {
			ret = document;
			ret.setContent(rs.getBytes("content"));
		}
		return ret;
	}
	
	@Override
	public ResultSet get() throws SQLException {
		return stmtGetAll.executeQuery();
	}

	@Override
	public boolean delete(int id) throws SQLException {
		setIdParam(stmtDelete, id);
		int affectedRows = stmtDelete.executeUpdate();
		stmtDelete.clearParameters();
		return (affectedRows == 0) ? false : true;
	}

	@Override
	public Object update(Object objToUpdate) throws SQLException {
		ScannyDocument doc = (ScannyDocument)objToUpdate;
		setParams(stmtUpdate, doc);
		stmtUpdate.executeUpdate();
		stmtUpdate.clearParameters();
		return objToUpdate;
	}
	
	private ScannyDocument parseObject(ResultSet rs) throws SQLException {
		ScannyDocument doc = new ScannyDocument();
		Category c = new Category();
		c.setId(rs.getInt("categoryid"));
		ScannyUser u = new ScannyUser();
		u.setId(rs.getInt("userid"));
		doc.setUser(u);
		doc.setCategory(c);
		//ToDo: load content
		return doc;
	}

	private void setIdParam(PreparedStatement statement, int id) throws SQLException {
		statement.setInt(1, id);
	}
	
	private void setParams(PreparedStatement statement, ScannyDocument document) throws SQLException {
		statement.setInt(1, document.getUser().getId());
		statement.setInt(2, document.getCategory().getId());
		statement.setBytes(3, document.getContent());
	}
}
