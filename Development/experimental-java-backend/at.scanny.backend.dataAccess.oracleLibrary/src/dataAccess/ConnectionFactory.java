package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;

import framework.databaseAccess.IConnectionFactory;

public class ConnectionFactory implements IConnectionFactory {
	public static ConnectionFactory instance = new ConnectionFactory();
	//ToDo: property file
	private final String ip = "192.168.128.152";
	private final String port = "1521";
	private final String sid = "ora11g";
	private final String user = "d5b03";
	private final String password = "d5b";
	
	private ConnectionFactory() {
		
	}
	
	private String getConnctionUrl() {
		return ip + ":" + port + ":" + sid;
	}
	
	@Override
	public Connection getConnection() throws Exception {
		Connection conn = null;
		Class.forName("oracle.jdbc.driver.OracleDriver"); 
		try {  
			 conn=DriverManager.getConnection(  
			"jdbc:oracle:thin:@" + getConnctionUrl(), user, password);
		} catch(Exception e) { 
			throw new Exception("Erro getting connection", e);
		}   
		return conn;
	}
}
