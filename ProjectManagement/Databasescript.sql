drop table ScannyUser cascade constraints;
drop table Category cascade constraints;
drop table ScannyDocument cascade constraints;
drop table Tag cascade constraints;
drop table UserDocumentShared cascade constraints;
drop sequence userid_seq;

create table ScannyUser(
nickname varchar2(20), 
firstname varchar2(20),
lastname varchar2(20),
password varchar2(20),
id number primary key
);

create sequence userid_seq;
create or replace trigger BeforeInsert_ScannyUser 
before insert on ScannyUser for each row begin 
 if :NEW.id is null then 
select userid_seq.nextval into :new.id from dual;
end if ;
end;
/


create table Category(
text varchar2(15), 
id number primary key
);

create sequence catid_seq;
create or replace trigger BeforeInsert_Category 
before insert on ScannyUser for each row begin 
 if :NEW.id is null then 
select catid_seq.nextval into :new.id from dual;
end if ;
end;


create table ScannyDocument(
content blob,
id number primary key,
userid number,
categoryid number,
foreign key(categoryid) references Category(id),
foreign key(userid) references ScannyUser(id)
);

create sequence scdocid_seq;
create or replace trigger BeforeInsert_Category 
before insert on ScannyDocument for each row begin 
 if :NEW.id is null then 
select scdocid_seq.nextval into :new.id from dual;
end if ;
end;
/

create table Tag(
text varchar2(10) primary key,
docid number, 
foreign key (docid) references ScannyDocument(id)
);

create table UserDocumentShared(
docid number,
userid number,
primary key(docid,userid),
foreign key (docid) references ScannyDocument(id),
foreign key(userid) references ScannyUser(id)
);

insert into ScannyUser values('Nick','peter','bugayev','secret',null);
insert into ScannyUser values('Kuntle','Klaus','Kunt','klaus2003',null);
select * from ScannyUser;