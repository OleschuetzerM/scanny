﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace at.delphin.ScannyLibrary
{
    public class ScannyLine
    {
        public string Text = null;
        public List<string> Words = new List<string>();
        public int HeaderType = -1;

        public static ScannyLine FromJToken(JToken token)
        {
            ScannyLine curLine = new ScannyLine();
            curLine.Text = token[Const.Text].ToString();
            var jWords = token[Const.LineWords];

            foreach (var kv in ScannyParams.Params)
            {
                if(Regex.Match(curLine.Text, kv.Key).Success)
                {
                    kv.Value(curLine);
                    curLine.Text = Regex.Replace(curLine.Text, kv.Key, "");
                    curLine.Text = curLine.Text.Trim();
                }
            }
            foreach (var jWord in jWords)
            {
                curLine.Words.Add(jWord[Const.Text].ToString());
            }
            return curLine;
        }
    }
}
