﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace at.delphin.ScannyLibrary
{
    public class ScannyResult
    {
        public string Text = "";
        public List<ScannyLine> Lines = new List<ScannyLine>();
        public string FileName = null;

        public static ScannyResult FromJToken(JToken token)
        {
            ScannyResult ret = new ScannyResult();
            var jLines = token[Const.Result][Const.Lines];
            string lineText = null;
            foreach (var jLine in token[Const.Result][Const.Lines])
            {
                lineText = jLine[Const.Text].ToString();
                if (ret.FileName == null && Regex.Match(lineText, "^#\\s#").Success || Regex.Match(lineText, "^#{2}").Success)
                {
                    ret.FileName = lineText;
                    ret.FileName = ret.FileName.Remove(0, 3);
                    ret.FileName = ret.FileName.Trim();
                    while (ret.FileName.Contains(" "))
                    {
                        ret.FileName = ret.FileName.Replace(' ', '_');
                    }
                }
                else
                {
                    ret.Text += jLine[Const.Text].ToString() + Environment.NewLine;
                    ret.Lines.Add(ScannyLine.FromJToken(jLine));
                }
            }
            return ret;
        }
    }
}
