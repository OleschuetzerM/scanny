﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using System.IO;

namespace at.delphin.ScannyLibrary.WordCreator
{
    internal static class WordManager
    {
        private static Random _Random = new Random();
        private static Application _Word = new Application();
        private static object _Missing = System.Reflection.Missing.Value;

        static WordManager()
        {
            _Word.ShowAnimation = false;
            _Word.Visible = false;
        }

        public static void CreateDocument(ScannyResult scannyResult)
        {
            Document document = _Word.Documents.Add(ref _Missing, ref _Missing, ref _Missing, ref _Missing);
            foreach (Section section in document.Sections)
            {
               Range headerRange = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                headerRange.Fields.Add(headerRange, WdFieldType.wdFieldPage);
                headerRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                headerRange.Font.ColorIndex = WdColorIndex.wdBlue;
                headerRange.Font.Size = 14;
                headerRange.Text = "Generated by 'Scanny'";
            }

            document.Content.SetRange(0, 0);

            foreach(var line in scannyResult.Lines)
            {
                CreateParagraph(document, line);
            }

            object filePath = "./";
            if (scannyResult.FileName == null)
                filePath += "cock " + _Random.Next(0, 100) + ".doc";
            else
                filePath += scannyResult.FileName + ".doc";
            filePath = Path.GetFullPath(filePath.ToString());
            document.SaveAs2(ref filePath);
            document.Close(ref _Missing, ref _Missing, ref _Missing);
            document = null;
        }

        public static void CreateParagraph(Document document, ScannyLine line)
        {

            Paragraph para = document.Content.Paragraphs.Add(ref _Missing);
            object header = document.Styles[WdBuiltinStyle.wdStyleNormal];
            if (line.HeaderType > 0)
            {
                header = "Überschrift " + line.HeaderType;
            }
            para.Range.set_Style(ref header);
            para.Range.Text = line.Text;
            para.Range.InsertParagraphAfter();
        }
    }
}
