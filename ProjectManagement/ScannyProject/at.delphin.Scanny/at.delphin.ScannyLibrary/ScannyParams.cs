﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace at.delphin.ScannyLibrary
{
    public static class ScannyParams
    {
        public static Dictionary<string, Action<ScannyLine>> Params = new Dictionary<string, Action<ScannyLine>>();

        static ScannyParams()
        {
            Params.Add("#\\s1\\s#", (line) => line.HeaderType = 1);
            Params.Add("#\\s2\\s#", (line) => line.HeaderType = 2);
            Params.Add("#\\s3\\s#", (line) => line.HeaderType = 3);
        }
    }
}
