﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace at.delphin.ScannyLibrary
{
    internal static class Const
    {
        public const string Result = "recognitionResult";
        public const string Lines = "lines";
        public const string LineWords = "words";
        public const string Text = "text";
    }
}
