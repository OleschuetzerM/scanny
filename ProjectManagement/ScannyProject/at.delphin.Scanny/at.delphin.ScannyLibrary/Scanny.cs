﻿using at.delphin.ScannyLibrary.WordCreator;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace at.delphin.ScannyLibrary
{
    public class Scanny
    {
        private const string _SubKey = "ac3e63ab63a541d8822d740410996efa";
        private const string _Uri = "https://westcentralus.api.cognitive.microsoft.com/vision/v2.0/recognizeText?mode=Handwritten";

        public async Task<ScannyResult> ProcessInput(byte[] byteData)
        {
            ScannyResult scannyResult = null;
            try
            { 
                HttpClient client = _CreateHttpClient();
                HttpResponseMessage response = await _PostContent(client, _Uri, byteData);
                string operationLocation;

                if (response.IsSuccessStatusCode)
                    operationLocation = response.Headers.GetValues("Operation-Location").FirstOrDefault();
                else
                {
                    string errorString = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("\n\nResponse:\n{0}\n",
                        JToken.Parse(errorString).ToString());
                    return null;
                }

                string contentString;
                int i = 0;
                do
                {
                    System.Threading.Thread.Sleep(1000);
                    response = await client.GetAsync(operationLocation);
                    contentString = await response.Content.ReadAsStringAsync();
                    ++i;
                }
                while (i < 10 && contentString.IndexOf("\"status\":\"Succeeded\"") == -1);

                if (i == 10 && contentString.IndexOf("\"status\":\"Succeeded\"") == -1)
                {
                    Console.WriteLine("\nTimeout error.\n");
                    return null;
                }

                JToken token = JToken.Parse(contentString);
                scannyResult = ScannyResult.FromJToken(token);
                Console.WriteLine(scannyResult.Text);
                WordManager.CreateDocument(scannyResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return scannyResult;
        }
        
        private HttpClient _CreateHttpClient()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add(
                "Ocp-Apim-Subscription-Key", _SubKey);
            return client;
        }
        
        private async Task<HttpResponseMessage> _PostContent(HttpClient client, string uri, byte[] data)
        {
            HttpResponseMessage response = null;
            using (ByteArrayContent content = new ByteArrayContent(data))
            {
                content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");

                response = await client.PostAsync(uri, content);
            }
            return response;
        }
    }
}
