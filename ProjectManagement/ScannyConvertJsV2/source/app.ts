import { Converter } from './converter/Converter';
import { ScannyResult } from './converter/ScannyResult';
import { documentRouter } from './routes/DocumentRoute';
import express = require("express");
import fileUpload = require("express-fileupload");

const port = 8080;

express()
    .use(fileUpload())
    .use("/documents", documentRouter)
    .listen(port, () => console.log(`Server is up and running on port ${port} . . . `));

