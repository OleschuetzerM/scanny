import * as request from "request";
import * as fs from "fs";
const sleep = (ms : number) => new Promise(resolve => setTimeout(resolve, ms));


export class Converter {
    public async processInput(path : string) : Promise<string> {
        var fileData = fs.readFileSync(path);
        return this.processInputBuffer(fileData);
    }
    
    public async processInputBuffer(buffer : Buffer) : Promise<string>{
        return new Promise(function(resolve, reject) {
            var options : request.Options = {
                url: ConverterConfig.subscriptionUri,
                headers: {
                    'Content-Type' : 'application/octet-stream',
                    'Ocp-Apim-Subscription-Key': ConverterConfig.subscriptionKey,
                },
                body: buffer
            };
            //var readStream = fs.createReadStream(path)            
            // readStream.pipe(request.post(options, function (error, response, body)
            // {
            //     if (!error && response.statusCode == 200) {
                   
            //        //Todo: parse response string here
            //         resolve();
            //     }
            //     else {
            //         reject(response.body);
            //     }
            // }));

            request.post(options, function (error, response, body)
            {
                if(error) {
                    reject(response.statusMessage);
                }
                else {
                    //can be String | String[], assumed that its only String
                    //ToDo: convert
                    var locationHeader : any = <String> response.headers['operation-location'];
                    setTimeout(function() {
                        var opt : request.Options = {
                            url: locationHeader,
                            headers: {
                                'Ocp-Apim-Subscription-Key': ConverterConfig.subscriptionKey
                            }
                        };
                        request.get(opt, function(err, res, bod)
                        {
                            if(error){
                                reject(res.statusMessage);
                            }
                            else {
                                resolve(bod);
                            }
                        });
                    
                    }, 5000);
                 
                    //reject(response.headers);
                }
            });
        });
    }
}


class ConverterConfig {
    public static subscriptionKey : String = "2e2d8a5facb1480bbe060abe444f2e4a";
    public static subscriptionUri  = "https://westcentralus.api.cognitive.microsoft.com/vision/v2.0/recognizeText?mode=Handwritten";
}