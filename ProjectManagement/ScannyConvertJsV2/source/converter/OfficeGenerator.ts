import { ScannyResult } from "./ScannyResult";
import { ScannyLine } from "./ScannyLine";
import { DebugFileSender } from "../debugConverter/debugFileSender";
import { ScannyWord } from "./ScannyWord";

const officegen = require('officegen');
const fs = require('fs');
const pathSystem = require("path");

export class OfficeGenerator {
  public async generate(path : string, scannyResult : ScannyResult) : Promise<string> {
    return new Promise(function(resolve, reject) {
        let docx = officegen('docx');
        var fileName = path + "\\scannyFile" + Date.now().toLocaleString() + ".docx";
    
       /* docx.on('finalize', function(written : any) {
          resolve(pathSystem.resolve(fileName));
        });*/
        
        docx.on('error', function(err : any) {
          reject(err);
        });

        scannyResult.scannyLines.forEach((e : ScannyLine)  => {
            let pObj = docx.createP();
            e.words.forEach((word : ScannyWord) => {
                let col : string = "";
                if(word.confidence !== undefined && word.confidence.toLowerCase() == "low") {
                    col = "ff0000";
                }
                else {
                    col = "000000";
                }
                pObj.addText(word.text + " ", { color: col })
            });
        });

   
        let out = fs.createWriteStream(fileName);
        
        out.on('error', function(err : any) {
            console.log(err);
            reject(err);
        });
        docx.generate(out);

        out.on('finish', () => {
          resolve(pathSystem.resolve(fileName));
        });
      }); 
    }
}
