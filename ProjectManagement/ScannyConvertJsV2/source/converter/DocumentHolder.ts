import { ScannyDocument } from './ScannyDocument';

export class DocumentHolder {
    public static documents: { [key: string]: ScannyDocument} = {};
}
