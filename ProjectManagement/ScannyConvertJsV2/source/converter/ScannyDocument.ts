export class ScannyDocument {
    constructor (
        public id: string,
        public filename: string
    ) {}
}