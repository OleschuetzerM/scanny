import { ScannyResultConsts } from './ScannyResultConsts';
import { ScannyLine } from './ScannyLine';
import { ScannyWord } from './ScannyWord';
import { OfficeGenerator } from './OfficeGenerator';

export class ScannyResult {
    public scannyLines : Array<ScannyLine> = new Array<ScannyLine>();

    public parseData(data : string) : void {
        let jsonData = JSON.parse(data);
        let lines = jsonData[ScannyResultConsts.result][ScannyResultConsts.lines];
        lines.forEach((element : any) => {
            let scannyLine : ScannyLine = new ScannyLine();
            scannyLine.text = element[ScannyResultConsts.lineText];
            element[ScannyResultConsts.lineWords].forEach((lineWord:any) => {
                let scannyWord = new ScannyWord();
                scannyWord.text = lineWord[ScannyResultConsts.lineText];
                scannyWord.confidence = lineWord[ScannyResultConsts.wordConfidence];
                scannyLine.words.push(scannyWord);
            });
            this.scannyLines.push(scannyLine);
        });
    }

    //path needs to include file ending (docx)
    public toWordFile(path : string, onResolved : Function, onError : Function) {
        var generator = new OfficeGenerator();
        generator.generate(path, this)
        .then((param) => {
            onResolved(param);
        }).catch((err) => {
            onError(err);
        });
    }
}