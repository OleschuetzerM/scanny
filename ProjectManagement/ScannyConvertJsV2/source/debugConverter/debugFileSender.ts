import * as request from "request";
import * as fs from "fs";

export class DebugFileSender {
    public async sendData(path : string) : Promise<string> {
        var fileData = fs.readFileSync(path);
        return this.sendDataBuffer(fileData);
    }

    public async sendDataBuffer(buffer : Buffer) : Promise<string>{
        return new Promise(function(resolve, reject) {
            var options : request.Options = {
                url: DebugRequestConfig.serverUri,
                //added header
                headers: { 
                    'Content-Type' : 'multipart/form-data'
                },
                //added json object for server
                body: JSON.stringify({
                    "file" : buffer
                })
            };    
            request.post(options, function (error, response, body)
            {
                if(error) {
                    reject(error);
                }
                else {
                    resolve(body);
                }
            });
        });
    }
}

class DebugRequestConfig { 
    public static serverUri  = "http://localhost:8080/documents";
}