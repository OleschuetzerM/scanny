import { Router, Request, Response } from "express";
import { UploadedFile } from "express-fileupload";
import { Converter } from '../converter/Converter';
import { ScannyResult } from '../converter/ScannyResult';
import * as fs from 'fs';
import { DocumentHolder } from "../converter/DocumentHolder";
import { ScannyDocument } from '../converter/ScannyDocument';

const router = Router();
let filecounter = 0;


router.get("/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    res.setHeader('Content-disposition', 'attachment; filename=scannydoc.docx');

    if (!req.params.id) return res.status(400).end("bad request");
    if (!DocumentHolder.documents[req.params.id]) return res.status(404).end("document not found");

    res.sendFile(DocumentHolder.documents[req.params.id].filename);
})

router.post("/", async function(req, res) {
    const converter: Converter = new Converter();
    const uploadedFile: UploadedFile = <UploadedFile>req.files.file;
    const scanResult = new ScannyResult();
    const data = await converter.processInputBuffer(uploadedFile.data);
    scanResult.parseData(data);

    scanResult.toWordFile("./", (filename: string) => {
        const doc = new ScannyDocument("" + filecounter++, filename);
        DocumentHolder.documents[doc.id] = doc;

        res.end(JSON.stringify({documentId: doc.id}));
    },
    () => {
        res.status(500).end("Internal Server Error");
        console.log("Error");
    });
});

export const documentRouter = router;