package cock.test.test;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class DocumentAdapter extends ArrayAdapter<Document> {
        //to reference the Activity
        private final Activity context;

        //to store the list of countries
        private final ArrayList<Document> infoArray;

    public DocumentAdapter(Activity context, ArrayList<Document> infoArrayParam){

        super(context,R.layout.listview_item , infoArrayParam);

        this.context=context;
        this.infoArray = infoArrayParam;

    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.listview_item, null,true);

        //this code gets references to objects in the listview_row.xml file
        //TextView nameTextField = (TextView) rowView.findViewById(R.id.nameTextViewID);
        //TextView infoTextField = (TextView) rowView.findViewById(R.id.infoTextViewID);
        //ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView1ID);

        //this code sets the values of the objects to values from the arrays
        //nameTextField.setText(infoArray.get(position).Title);
        //infoTextField.setText(infoArray.get(position).Tags.toString());

        return rowView;

    };
}
